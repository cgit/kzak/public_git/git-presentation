% vim: set tabstop=4:
% vim: set shiftwidth=4:
% vim: set expandtab:

\RHpresentationHead{
    \documentclass[pdftex,unicode,xcolor=table,slidestop,compress]{beamer}
}

\RHarticleHead{
    % This does not work, because of colors, \insertauthor, etc.
    \documentclass[a4paper,12pt,pdftex,unicode]{article}
    \usepackage[envcountsect]{beamerarticle}
}

%\usepackage{pgfpages}
\usetheme{Git}
\setbeamertemplate{navigation symbols}{}
\setbeamercovered{transparent=5}

\usepackage[utf8]{inputenc}
%\usepackage[lang]{babel}
\usepackage{setspace,amsfonts,calc,upquote,hyperref,floatflt,graphicx}
\usepackage[table]{xcolor}
\usepackage{colortbl}
\usepackage[absolute,overlay]{textpos}

% Automatically create section page
\newcommand{\sectionpage}{\usebeamertemplate*{section page}}
\AtBeginSection[]%
{
    \begin{rhbg}
        \frame{\sectionpage}
    \end{rhbg}
}

\defverb\verbemail|Random Hacker <rand@hacker.com>|
\defverb\kzakemail|<kzak@redhat.com>|

% presentation title/author/etc.
\title{Git}
\subtitle{(Why not CVS? ... because Git.)}
\author{Karel Zak \and\\
Florian Festi \and\\
Bart Trojanowski}


\begin{document}

\begin{rhbg}
    \begin{frame}
        \titlepage
    \end{frame}
\end{rhbg}

\begin{frame}[plain]
\vspace*{15ex}
Copyright \copyright{}\ \ \the\year\ \ Karel Zak\\
Copyright \copyright{}\ \ \the\year\ \ Tomas Janousek (beamer template)\\
Copyright \copyright{}\ \ \the\year\ \ Florian Festi\\
Copyright \copyright{}\ \ \the\year\ \ Bart Trojanowski\\
\vspace*{2ex}
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover
Texts.
\par\vspace*{5ex}
\scriptsize{ Source code: http://kzak.fedorapeople.org/git-presentation.git }
\end{frame}

% table of contents
\begin{frame}
    \frametitle{Agenda}
    \tableofcontents
\end{frame}

\section{Intro}

\begin{frame}
    \frametitle{What is Git?}
    \begin{quote}
    ``I'm an egotistical bastard, and I name all my projects after myself. First 'Linux', now 'git'.'' (Linus Torvalds)
    \end{quote}
    \vspace*{4ex}
    \begin{itemize}
        \item fast distributed revision control system
        \item unusually rich command set
        \item provides both high-level operations and full access to internals
        \item originally created by Linus Torvalds for kernel development
        \item design was inspired by BitKeeper and Monotone
        \item GNU General Public License, version 2
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Basic features}
    \begin{itemize}
        \item distributed development model
        \item support for non-linear development (branching and merging)
        \item ready for large projects (very good performance)
        \item repositories can be easily published (git://, ssh://, http://, rsync://, ...)
        \item cryptographic authentication of history (GPG-signed tags)
        \item internally objects are addressed by content (SHA-1) -- not filenames
        \item local branches are local only (off-line work)
    \end{itemize}
\end{frame}

\subsection{Development model}

\begin{frame}
    \frametitle{Centralized model}
    \includegraphics[width=7cm,height=4cm]{git-centralized.pdf}
    \begin{itemize}
        \item extra policy for write access
        \item SCM is not development tool, but source code archive only
        \item every change has impact to all developers
        \item no private branches
        \item needs connection to server for most operations
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Distributed model}
    \includegraphics[width=8cm,height=5cm]{git-distributed.pdf}
    \begin{itemize}
        \item maintainer has full control over primary (his) repository
        \item support for non-linear development
        \item repositories can be easily published (git://, ssh://, http://, ...)
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Git improves your work manners and habits}
    \begin{itemize}
        \item
            branching and merging is cheap

        \begin{itemize}
            \item you can prototype
            \item you can collaborate with others developers on incomplete and unstable stuff
            \item you can easily (e.g. every day) rebase your changes to new upstream code
            \item merge (rebase) often minimizes conflicts between your patches and upstream
        \end{itemize}

        \item
            small patch is the best patch (patch per feature/change)

        \begin{itemize}
            \item reviewers hate huge patches
            \item well separated feature or change is easy to revert
            \item per item commit messages
        \end{itemize}
        \item much less dependent on your patches going in upstream
        \item manage patches - not just store them
    \end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
    \frametitle{Workflow}
    Changes go through several stages before ending up in their final destination
    \begin{description}[master branch]
        \item[working dir] current checkout - editing happens here
        \item[index] aka ``cache'' aka ``staging area'' - changes that are selected to be commited
        \item[commit] now packaged up with a commit message and part of the history
        \item[master branch] move the commits over when the feature is finished
        \item[origin] get the changes upstream
    \end{description}
\end{frame}

\subsection{Commands}

\begin{frame}[containsverbatim]
    \frametitle{Syntax}
    \begin{itemize}
        \item \texttt{git <commandname> [options]}
        \item \texttt{git-<commandname> [options]}
        \item \texttt{man git-<commandname>}
        \item \texttt{git help <commandname>}
    \end{itemize}

    \begin{exampleblock}{High level commands: Porcelain}
    	\begin{verbatim} $ git commit -a -s -m "cool change" \end{verbatim}
    \end{exampleblock}
    \begin{exampleblock}{Low level commands: Plumbing}
    	\begin{verbatim} $ git rev-list --pretty=oneline v2.13.. \end{verbatim}
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Basic commands (local)}
    \begin{description}[git commit]
        \item[git init] creates en empty repository at \texttt{./.git}
        \item[git add] adds file contents to the next commit
        \item[git rm] removes a file
        \item[git mv] move a file
        \item[git status] shows the working tree status
        \item[git commit] records changes to the repository
        \item[git log] shows commit log
        \item[git show] shows commit (or another object)
    \end{description}
\end{frame}

\begin{frame}
    \frametitle{Basic commands (remote)}
    \begin{description}[git format-patch]
        \item[git fetch] get new changes from external repository
        \item[git pull] fetch + merge
        \item[git push] write new changes to external repository
        \item[git format-patch] exports a change
        \item[git send-email] sends patch(s)
        \item[git am] applies a series of patches from a mailbox
    \end{description}
\end{frame}

\begin{frame}
    \frametitle{Advanced Commands (local)}
     \begin{description}[git cherry-pick]
        \item[git branch] create/modify/delete branches
        \item[git checkout] switch work dir to another branch/commit
        \item[git merge] merge two or more branches
        \item[git rebase] changes starting point of a branch
        \item[git cherry-pick] copy patch from another branch
        \item[git reset] set back a branch HEAD
        \item[git bisect] find the breaking patch
        \item[git stash] save/restore current work dir changes
        \item[git gc] compactify repository and do clean ups
    \end{description}
\end{frame}

\section{Implementation}
\subsection{Internal objects}

\begin{frame}
    \frametitle{Internal objects}
    \begin{block}{}
       All objects are content-addressable by SHA-1.
    \end{block}

    \begin{description}[references]
        \item[commit] refers to ``tree'' and ``parent'' (connection into the project history) and contains the commit message
        \item[tree] represents the state of a single directory (list of ``blob'' objects and subtrees)
        \item[blob] contains file data without any other structure
    \end{description}
\end{frame}

\begin{frame}
    \frametitle{Internal objects}
    \includegraphics[width=8cm,height=4cm]{git-objdb.pdf}
    \begin{description}[commit]
        \item[commit] -- connection between ``tree'' and ``parent``
        \item[tree] -- state of a single directory
        \item[blob] -- contain file data
    \end{description}
\end{frame}

\begin{frame}
    \frametitle{References}
    \begin{itemize}
        \item Tag
        \begin{itemize}
                \item contains SHA-1 sum of a commit
                \item may contain an explaining message
                \item can be PGP-signed
                \item stays fix
                \item .git/refs/tags
        \end{itemize}
        \item Branch
        \begin{itemize}
                \item SHA-1 sum of a commit
                \item ``leaf'' of the history ``tree''
                \item follows the commits to that branch
                \item .git/refs/heads
        \end{itemize}
        \item tracked branches - .git/refs/remotes/ origin
        \item HEAD - the current branch 
        \item ORIG\_HEAD - HEAD before the last reset
    \end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
    \frametitle{Trust}
    \begin{itemize}
        \item everything is content-addressed and based on SHA-1
        \item two trees are same when HEAD SHA-1 are same
        \item SHA-1 summ are checked to verify data integrity
        \item content, history and commit messages can be signed by only GPG-signing one tag
    \end{itemize}

    \begin{exampleblock}{}
    \scriptsize{
    	\begin{verbatim}
 $ git tag -v v2.13
 object 49ef7acdf77066ed05a6c828c261d332c4f54644
 type commit
 tag v2.13
 tagger Karel Zak <kzak@redhat.com> Tue Aug 28 01:01:35 2007 +0200

 stable release v2.13
 gpg: Signature made Tue 28 Aug 2007 01:01:35 AM CEST using DSA key ID DC06D885
 gpg: Good signature from "Karel Zak <kzak@redhat.com>" \end{verbatim}
    }
    \end{exampleblock}
\end{frame}


\subsection{Naming revisions}

\begin{frame}[containsverbatim]
    \frametitle{Object reference}
    \defverb\ngrandparent|commit~n|
    \defverb\nparent|commit^n|
    \begin{description}[ref@\{date\}]
        \item[SHA-1] 40-hexdigit object name
        \item[tag] human readable name for commit
        \item[\nparent] N-th parent
        \item[\ngrandparent] N-th generation grand-parent of the named commit object, following only the first parent.
        \item[ref@\{date\}] specify the value of the ref at a prior point in time
        \item[:/text] commit whose commit message starts with the specified text
        \item[HEAD] refers to the head of the current branch
    \end{description}

    \begin{exampleblock}{}
        \begin{verbatim} rev~3   =    rev^^^    =   rev^1^1^1 \end{verbatim}
    \end{exampleblock}

    \begin{exampleblock}{}
        \begin{verbatim} $ git reset HEAD^ \end{verbatim}
    \end{exampleblock}
\end{frame}

\begin{frame}[containsverbatim]
    \frametitle{Ranges}
    \begin{description}[r1...r2]
        \item[r1..r2] commits reachable from r2 but exclude the ones reachable from r1
        \item[r1...r2] set of commits that are reachable from either one of r1 or r2 but not from both
    \end{description}

    \begin{exampleblock}{}
        \begin{verbatim} $ git log v2.13..v2.14 \end{verbatim}
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{"tree-ish"}
    Lots of commands take a tree as an argument. A tree can be referred to in many different ways, by:

    \begin{itemize}
        \item name for that tree
        \item name of a commit that refers to the tree
        \item name of a branch whose head refers to that tree
    \end{itemize}
\end{frame}

\section{Getting started}
\subsection{Configuration}

\begin{frame}[containsverbatim]
    \frametitle{Configuration}
    global configuration is in $\sim$/.gitconfig
    \begin{exampleblock}{}
    \begin{verbatim} $ git config --global --list
    user.name=Florian Festi
    user.email=ffesti@redhat.com
    diff.color=auto\end{verbatim}
    \end{exampleblock}
    repository configuration is in repo/.git/config
    \begin{exampleblock}{}
    \begin{verbatim} $ git config --list\end{verbatim}
    \end{exampleblock}
    changing settings
    \begin{exampleblock}{}
    \begin{verbatim} $ git config --global user.name "Florian Festi"
 $ git config --global user.email ffesti@redhat.com \end{verbatim}
    \end{exampleblock}
\end{frame}

\begin{frame}[containsverbatim]
    \frametitle{.gitconfig}
simple sample config
    \begin{exampleblock}{}
    \begin{verbatim}[user]
        name= Florian Festi
        email = ffesti@redhat.com
[diff]
        color = auto
\end{verbatim}
\end{exampleblock}
see \texttt{man git-config} for all config options
\end{frame}

\begin{frame}[containsverbatim]
    \frametitle{Create a repository}

    \begin{itemize}
        \item create a new repository
            \begin{exampleblock}{}
                \small{
                \begin{verbatim}
 $ mkdir project
 $ cd project
 $ git init \end{verbatim}
            }
             \end{exampleblock}
        \item clone an existing remote repository ("origin" repository)
            \begin{exampleblock}{}
                \small{ \begin{verbatim} $ git clone http://foo.com/project \end{verbatim} }
            \end{exampleblock}
        \item add a next remote repository
            \begin{exampleblock}{}
                \small{
                \begin{verbatim} $ git config remote.bar.url git://bar.com/project
 $ git config remote.bar.fetch master:refs/remotes/bar/master
 $ git fetch bar \end{verbatim}
                }
            \end{exampleblock}
    \end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
     \frametitle{Repository config file}
    \begin{exampleblock}{}
    \begin{verbatim}
[core]
        repositoryformatversion = 0
        filemode = true
        bare = false
        logallrefupdates = true
[remote "origin"]
        url = ssh://login.linux.duke.edu/.../yum.git
        fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
        remote = origin
        merge = refs/heads/master
\end{verbatim}
\end{exampleblock}
\end{frame}


\subsection{Visualisation}

\begin{frame}
    \frametitle{Visualisation}
    \begin{itemize}
        \item visualization helps when working with branches
        \item http://git.or.cz/gitwiki/InterfacesFrontendsAndTools
    \end{itemize}
History viewer:
    \begin{description}
        \item[gitk] Tcl/Tk History Viewer
        \item[qgit] Qt History Viewer, patch import/export
        \item[gitweb] Web front end (CGI, mod\_perl) 
    \end{description}
Commit tools
    \begin{description}
        \item[git gui] Tcl/Tk, builtin
    \end{description}
\end{frame}

\begin{frame}
    \frametitle{Visualisation: gitk}
    \includegraphics[width=0.9\textwidth]{gitk.png}
\end{frame}

\begin{frame}
    \frametitle{Visualisation: qgit}
    \includegraphics[width=0.9\textwidth]{qgit.png}
\end{frame}

\begin{frame}
    \frametitle{Visualisation: Gitweb}
	\includegraphics[width=0.9\textwidth]{gitweb.png}
\end{frame}

\begin{frame}[containsverbatim]
    \frametitle{Browsing changes}
    \begin{description}[git whatchanged]
        \item[git log] shows commit logs
        \item[git show] shows one or more objects (blobs, trees, tags and commits)
        \item[git blame] shows what revision and author last modified each line of a file
        \item[git whatchanged] shows logs with difference each commit introduces
    \end{description}
    \begin{exampleblock}{}
    \begin{scriptsize}
        \begin{verbatim}
$ git log v2.5..                     # commits since v2.5
$ git log test..master               # commits reachable from master
                                     #         but not test
$ git log --since="2 weeks ago"      # commits from the last 2 weeks
$ git log Makefile                   # commits which modify Makefile
$ git log --pretty=format:"%h [%an]" # commit log in format
                                     #        "sha-1 [Author Name]"
$ git blame -L 10,15 foo.c           # who modified code between lines
                                       10 and 15
$ git show c1a47c171b                # shows selected object (commit) \end{verbatim}
    \end{scriptsize}
    \end{exampleblock}
\end{frame}

\section{Branches}

\begin{frame}[containsverbatim]
    \frametitle{Branches}
    \begin{exampleblock}{}
        \begin{verbatim}
         o--o--o   <-- Branch A
        /
 o--o--o--o--o     <-- master
        \
         o--o--o   <-- Branch B  \end{verbatim}
    \end{exampleblock}

    \begin{description}[branch head]
        \item[branch] is line of development
        \item[branch head] is a reference to the most recent commit on a branch
    \end{description}
    \begin{itemize}
        \item branches become remote branches when cloning a repository
        \item use \texttt{git branch -a} (all) or \texttt{-r} (remote) to see the remote branches  
    \end{itemize}

\end{frame}

\begin{frame}[containsverbatim]
    \frametitle{Manipulating branches}
    \defverb\branchname|<branch>|
    \begin{itemize}
        \item \structure{git branch} lists, creates, or deletes branches
        \item \structure{git checkout \branchname} makes the current branch \branchname, updating the working directory
        \item \structure{git checkout -b \branchname} creates a new branch \branchname check it out
        \item \structure{git show-branch} shows branches and their commits
        \item \structure{git diff \branchname..\branchname} diffs between branches
    \end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
    \frametitle{Merge branch}
    \begin{exampleblock}{Before}
        \begin{verbatim}
                 A---B---C topic
                /
           D---E---F---G master \end{verbatim}
    \end{exampleblock}

    \begin{exampleblock}{Command}
        \begin{verbatim} $ git merge topic \end{verbatim}
    \end{exampleblock}

    \begin{exampleblock}{After}
        \begin{verbatim}
                 A---B---C topic
                /         \
           D---E---F---G---H master \end{verbatim}
    \end{exampleblock}
\end{frame}

\begin{frame}[containsverbatim]
    \frametitle{Rebase branch}
    \begin{exampleblock}{Before}
        \begin{verbatim}
                 A---B---C topic
                /
           D---E---F---G master \end{verbatim}
    \end{exampleblock}

    \begin{exampleblock}{Command}
        \begin{verbatim} $ git rebase master topic \end{verbatim}
    \end{exampleblock}

    \begin{exampleblock}{After}
        \begin{verbatim}
                         A---B---C topic
                        /
           D---E---F---G master \end{verbatim}
    \end{exampleblock}
    \begin{exampleblock}{Alternative}
        \begin{verbatim} $ git rebase -i topic \end{verbatim}
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Merge vs Rebase}
    \begin{itemize}
        \item Merge
        \begin{itemize}
            \item does an 3-way merge (for simple cases)
            \item leads to non linear history
            \item merging several branches with each other looks messy
            \item keeps separate branches visible
            \item Use in public repositories!
        \end{itemize}
        \item Rebase
        \begin{itemize}
            \item reapplies the patches on top
            \item alters history - new patches with new SHA-1 sums
            \item breaks work based on that branch
            \item therefore not suited for published work
            \item allows creating ``the perfect patch'' against upstream
            \item Use for private work!
        \end{itemize}
        \item Read the man pages for details!
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Resolve Conflicts}
    \begin{itemize}
    \item Read the messages!
    \item resolved stuff gets added to the index
    \item conflicts are applied to the work dir only
    \item resolve and add to index
    \item merge: \texttt{commit}
    \item rebase: \texttt{--continue}, \texttt{--abort} or \texttt{--skip} 
    \end{itemize}
\end{frame}

\section{Real life with Git}
\subsection{Changing History}

\begin{frame}[containsverbatim]
    \frametitle{Edit 3rd commit from the top}
    \begin{enumerate}
        \item Working on branch master
        \begin{exampleblock}{}                                                                     
        \begin{verbatim}
             A--B--C--D--E(master)\end{verbatim}
        \end{exampleblock}
        \item realized you made a mistake in commit 'B'
        \begin{exampleblock}{}
        \begin{verbatim}  $ git checkout HEAD~3
  $ git commit --amend
             .B'(HEAD)
            /
           A--B--C--D--E(master)\end{verbatim}
        \end{exampleblock}
        \item bring back the other commits
        \begin{exampleblock}{}                                                                     
        \begin{verbatim} $ git rebase HEAD master
             A--B'--C--D--E(master)\end{verbatim}
        \end{exampleblock}
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Changes in project history}
    \begin{itemize}
        \item the very last patch -- \texttt{"git commit --amend"} to add changes to last commit
        \item the latest patches -- \texttt{"git reset"} to remove the last commits from the history
        \item organize your own branch
            \begin{itemize}
                \item \texttt{"git cherry-pick"} patch per patch into a new branch
                \item \texttt{"git rebase -i"} to freely reorder patches
            \end{itemize} 
        \item deep in project history
            \begin{itemize}
                \item \texttt{"git rebase"} to move around large part of the history
                \item \texttt{"git revert"} to add a reversed patch on top
            \end{itemize}
    \end{itemize}
\end{frame}

\subsection{Handling Patches}

\begin{frame}
    \frametitle{Send a patch}
    Basic rules:
    \begin{itemize}
        \item \structure{one patch per e-mail}
        \item don't use stupid e-mail clients (e.g. Outlook)
        \item \structure{don't use attachments}
        \item export patches by \texttt{git format-patch}
        \item send patches by \texttt{git send-email}

        \item well formatted patch is possible to apply by \texttt{git am}

        \item don't forget to \structure{keep correct authorship} (e.g when you are not author of the patch)

        \item \structure{use commit messages} -- a patch without comment is incomplete crap
    \end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
     \frametitle{Export patches to files}
     \begin{block}{}
        \begin{verbatim} git format-patch [options] <since|range> \end{verbatim}
     \end{block}
     \begin{itemize}
        \item creates one file per patch
        \item created patches are usable by \structure{git am}
    \end{itemize}
    \begin{exampleblock}{}
    \scriptsize{
        \begin{verbatim}
$ git format-patch -o ~/ HEAD~5
/home/kzak/0001-setterm-opened-file-leaving-unclosed.patch
/home/kzak/0002-sfdisk-opened-files-leaving-unclosed.patch
/home/kzak/0003-blockdev-fix-opened-file-leaving-unclosed.patch
/home/kzak/0004-tailf-opened-file-leaving-unclosed.patch
/home/kzak/0005-tests-use-losetup-s.patch \end{verbatim}
    }
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Patch description}
	\includegraphics[width=0.9\textwidth]{git-patch.pdf}
\end{frame}

\begin{frame}[containsverbatim]
     \frametitle{Send patches by e-mail}
     \begin{block}{}
         \begin{verbatim} git send-email [options] <file|dir> \end{verbatim}
     \end{block}
     Takes the patches given on the command line and emails them out.\\
     \begin{itemize}
        \item no attachments
        \item no broken patch format
        \item correct subject line
     \end{itemize}
     \begin{exampleblock}{}
        \begin{verbatim}
$ git send-email --to "God <father@heaven.com>" \
                 ~/0001-make-this-world-better.patch \end{verbatim}
    \end{exampleblock}
\end{frame}


\section{Misc}


\begin{frame}
  \frametitle{References}
  \begin{thebibliography}{Git User's Manua}
  \bibitem[Git User's Manual]{GitUserManual}
    Git User's Manual
    \newblock {\url{http://www.kernel.org/pub/software/scm/git/docs/user-manual.html}}
  \bibitem[Git Tutorial]{GitTutorial}
    A tutorial introduction to git
    \newblock {\url{http://www.kernel.org/pub/software/scm/git/docs/tutorial.html}}
  \bibitem[The perfect patch]{PerfectPatch}
    The perfect patch
    \newblock {\url{http://www.zip.com.au/~akpm/linux/patches/stuff/tpp.txt}}
  \end{thebibliography}
\end{frame}

\begin{frame}[containsverbatim]
    \hfill
    \begin{center}
    {\Huge The end.}
    \par\vspace*{0.5cm}
    Thanks for listening.
    \hfill
    \par\vspace*{5cm}
    \url{http://kzak.fedorapeople.org/git-presentation.pdf}
    \end{center}
\end{frame}

\end{document}


